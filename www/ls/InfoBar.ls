maxRatio = 0.0361

class ig.InfoBar
  (@parentElement) ->
    @element = @parentElement.append \div
      .attr \class \info-bar
    @heading = @element.append \h2
    @list = @element.append \ol

  drawList: ({properties}:feature) ->
    {countries} = properties
    population = properties.obyv_celke
    @heading.html properties.NAZOK.replace '-' ' – '
    @listItems = @list.selectAll \li .data countries, (.name)
      ..enter!append \li
        ..append \span
          ..attr \class \name
          ..html (.name)
        ..append \span
          ..attr \class \value
        ..append \div
          ..attr \class \bar
      ..exit!remove!
      ..select \.value
        ..html ->
          "<b>#{ig.utils.formatNumber it.total}</b> lidí<br>
          <b>#{ig.utils.formatNumber it.total / population * 1000, 1}</b> na 1 000 obyvatel"
      ..style \top -> "#{it.index * 59}px"
      ..select \.bar
        ..style \width -> "#{(it.total / population) / maxRatio * 100}%"

