container = d3.select ig.containers.base
geojson = topojson.feature ig.data.okresy, ig.data.okresy.objects."data"
featuresAssoc = {}
totalPopulation = 0
for feature in geojson.features
  featuresAssoc[feature.properties.NAZOK] = feature
  feature.properties.total = 0
  feature.properties.ratio = 0
  feature.properties.countries = []
  totalPopulation += feature.properties."obyv_celke"
lines = ig.data.data.split "\n"
okresy = lines.0.split "\t"
countrySums = []
countrySumsAssoc = {}
for line in lines.slice 2
  fields = line.split "\t"
  countryName = fields[0]
  if countryName
    countrySums.push {name: countryName, total: 0}
  type = fields[1]
  for okresFieldIndex in [2 til fields.length by 3]
    okres = featuresAssoc[okresy[okresFieldIndex]]
    people = (parseInt fields[okresFieldIndex + 2], 10) || 0
    okres.properties.total += people
    okres.properties.ratio += people / okres.properties."obyv_celke"
    countrySums[*-1].total += people
    if countryName
      okres.properties.countries.push {name: countryName, total: 0}
    okres.properties.countries[*-1].total += people
    okres.properties.countries[*-1][type] = people

for feature in geojson.features
  feature.properties.countries.sort (a, b) -> b.total - a.total
  for country, index in feature.properties.countries
    country.index = index

countrySums.sort (a, b) -> b.total - a.total
for country, index in countrySums
  country.index = index

sumCountry =
  properties:
    NAZOK: "Česká republika"
    obyv_celke: totalPopulation
    countries: countrySums

infoBar = new ig.InfoBar container
map = new ig.Map container, geojson, infoBar, sumCountry
infoBar.drawList sumCountry
